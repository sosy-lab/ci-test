# SoSy-Lab CI Test and Info Project

This project is intended for testing our CI config for GitLab
and gathering information about the various possible scenarios:

1. pushes to main ([example pipeline](https://gitlab.com/sosy-lab/ci-test/-/pipelines/1067954476))
2. pushes to another branch in this project (no MR open) ([example pipeline](https://gitlab.com/sosy-lab/ci-test/-/pipelines/1066838921))
3. pushes to another branch in this project with an open MR ([example pipeline](https://gitlab.com/sosy-lab/ci-test/-/pipelines/1066848325))
4. pushes to main in a fork ([example pipeline](https://gitlab.com/BaierD/ci-test/-/pipelines/1067955112))
5. pushes to another branch in a fork (no MR open) ([example pipeline](https://gitlab.com/BaierD/ci-test/-/pipelines/1067860887))
6. pushes to another branch in a fork with an open MR against this project ([example pipeline](https://gitlab.com/BaierD/ci-test/-/pipelines/1067958431))
7. pipelines triggered manually for an MR from a fork ([example pipeline](https://gitlab.com/sosy-lab/ci-test/-/pipelines/1067961767))

There are even more possible scenarios (tags, schedules, manual triggers),
but these generally behave like 1. and are not explicitly tested.
Note that for case 6. it matters that the MR author is not a member of the target project.

Each test commit here is supposed to add a file to `commits/` with a descriptive name
that starts with the number of the scenario above,
such that we can see whether `git diff` works as desired.

The following CI jobs are used:
- `main-variables`: Shows environment variables for pushes to `main`.
- `branch+mr-variables`: Shows environment variables for pushes to branches and MRs.
- `branch+mr-git-info`: Shows information about the state of the git repository on the runner (existing references, checked out commit, etc.).
- `branch+mr-git-diff-origin/main`: Shows output from `git diff origin/main...` after doing `git fetch origin/main`.
- `mr-git-diff-variables`: Shows output from `git diff` using environment variables provided by GitLab CI for finding the merge base (only in MRs).

This project is configured with [merged results pipelines](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html)
and the CI config enables [merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html).
Redundant push/MR pipelines are [disabled](https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines).

## Findings

- CI jobs are always run with "detached" HEAD.
- Besides `HEAD`, MR pipelines with "merged results" have no useful git refs present, other pipelines have the respective branch ref present.
- MRs created from a fork, but by project members behave like internal MRs.
  Their pipelines immediately run in the context of the parent project, not the fork.
  An example for this is !2, where the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/2/pipelines)
  look like the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/1/pipelines) from !1.
- MRs create from a fork by non-members behave differently.
  Pipelines for such MRs are executed in the context of the fork,
  and do not use the "merged results" feature,
  as can be seen for the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/3/pipelines) for !3.
  Only once a project member triggers a pipeline using the "Run pipeline" button
  a pipeline in the context of the parent project and with "merged results" is created.
- The variable `CI_MERGE_REQUEST_DIFF_BASE_SHA` is present in all MR pipelines
  (no matter whether from a fork or not and whether "merged results" is active)
  and designates the appropriate base for computing the diff of the MR
  (what git calls the "merge base").
  So to compute the same diff as GitLab shows in the UI,
  use `git diff $CI_MERGE_REQUEST_DIFF_BASE_SHA`.
  This does not require any history to be present (checkout depth is irrelevant),
  but a `git fetch origin $CI_MERGE_REQUEST_DIFF_BASE_SHA` may be necessary.
